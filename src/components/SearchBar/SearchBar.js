/**
 * Created by timur on 20.02.17.
 */
import React, {  PropTypes} from 'react';
import styles from './SearchBar.css';

const SearchBar = (props) => (
        <div className={styles.root} >
            <input  className={styles.input}
            onChange={ (e) => props.updateText(e.target.value)} />
            <button
            className={styles.button}
            onClick={props.fetchSongs}
            >Get songs</button>
        </div>
    );


SearchBar.propTypes = {
    fetchSongs: PropTypes.func,
  updateText:  PropTypes.func
};
export default SearchBar;